import os
import json
import uuid
import pickle
from subprocess import call
from collections import defaultdict

import click

from api.app import app
from api.models import db


@click.group()
def cli():
    pass


@cli.command()
def run_api():
    app.run(
        host=os.environ.get('HOST', 'localhost'),
        debug=True
        # os.environ.get('DEBUG', True)
    )


@cli.command()
def run_celery():
    call('celery -A api.celery worker')


@cli.command()
def create_db():
    db.create_all()


@cli.command()
def generate_secret():
    with open('secret.txt', 'w') as f:
        f.write(str(uuid.uuid4()))

# This code should be migrated to new metabolitics packages
# @cli.command()
# def generate_angular_friendly_model():
#     '''
#     This function convert json model into angular friendly json
#     '''
#     model = DataReader().read_network_model()
#     model_json = json.load(open('../dataset/network/recon2.json'))

#     reactions, metabolites = model_json['reactions'], model_json['metabolites']
#     model_json = defaultdict(dict)
#     model_json['pathways'] = defaultdict(list)

#     for m in metabolites:
#         m['reactions'] = [
#             r.id for r in model.metabolites.get_by_id(m['id']).reactions
#         ]
#         model_json['metabolites'][m['id']] = m

#     for r in reactions:
#         # r['gene_reaction_rule'], r['notes'] = [], {}
#         del r['gene_reaction_rule']
#         del r['notes']

#         model_json['reactions'][r['id']] = r
#         model_json['pathways'][r.get('subsystem', 'NOpathway')].append(r['id'])

#     json.dump(model_json, open('../outputs/ng-recon.json', 'w'))


# @cli.command()
# def healties_model():
#     X, y = DataReader().read_healthy('BC')

#     pre_model = DynamicPreprocessing(['naming', 'basic-fold-change-scaler'])
#     X = pre_model.fit_transform(list(X), y)

#     model = DynamicPreprocessing(['fva', 'flux-diff'])
#     model.fit(X, y)

#     with open('../outputs/api_model.p', 'wb') as f:
#         pickle.dump(model, f)


if __name__ == '__main__':
    cli()
